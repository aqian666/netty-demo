package com.chat.comment;

import com.alibaba.fastjson.JSONObject;
import com.chat.netty.websocketserver.service.ChatService;

import com.chat.netty.websocketserver.vo.ResultMsg;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;


@Component
public class Task {

    @Resource
    private ChatService chatService;

    @Scheduled(cron="0/59 * *  * * ? ") // 每5秒执行一次
    public void execute() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String s = "欢迎来到 答案的聊天室 " + df.format(new Date());
        JSONObject param = new JSONObject();
        param.put("msg",s);
        param.put("type","all");
        chatService.pushMsgToAll(param);
    }




}
