package com.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling //开启定时任务
@SpringBootApplication
public class NettyDemoApplication {

    public static void main(String[] args) {
//        System.setProperty("https.protocols", "TLSv1.2,TLSv1.1,TLSv1.0,SSLv3");
//        System.setProperty("javax.net.debug", "all");
        SpringApplication.run(NettyDemoApplication.class, args);
    }

}
