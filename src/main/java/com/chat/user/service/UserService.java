package com.chat.user.service;

import com.chat.user.dao.UserDao;
import com.chat.user.model.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService {

    @Resource
    private UserDao userDao;

    //登录
    public User findByUsernameAndPassword(String username, String password) {
        return userDao.findByUsernameAndPassword(username, password);
    }

    //注册
    public void save(User user1) {
        userDao.save(user1);
    }

    //注册时差询名字是否重复
    public List<User> findByUsername(String username) {
        return userDao.findByUsername(username);
    }

}
