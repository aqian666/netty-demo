package com.chat.netty.websocketserver.controller;

import com.chat.netty.websocketserver.service.ChatService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController("/chat")
public class ChatController {

    @Resource
    private ChatService chatService;


    @PostMapping("/pushAll")
    public void pushToAll(@RequestParam("msg") String msg){
        chatService.pushMsgToAll(msg);
    }

    /**
     * 推送给指定用户
     * @param userId 用户ID
     * @param msg 消息信息
     */
    @PostMapping("/pushOne")
    public void pushMsgToOne(@RequestParam("userId") String userId,@RequestParam("msg") String msg){
        chatService.pushMsgToOne(userId,msg);
    }


}
