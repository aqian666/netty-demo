package com.chat.netty.websocketserver.vo;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResultMsg{

    private static final Integer SUCCESS_STATUS = 200;
    private static final Integer ERROR_STATUS = -1;
    private static final String SUCCESS_TYPE = "NOT_ALL";

    private String content;

    private Integer status;
    private String type;

    private Map<String,String> names;

    private Date date=new Date();

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public void setContent(String name,String msg) {
        this.content = name+" "+DateFormat.getDateTimeInstance().format(date) +":<br/> "+msg;
    }

    public Map<String, String> getNames() {
        return names;
    }
    public void setNames(Map<String, String> names) {
        this.names = names;
    }


    public String toJson(){
        return gson.toJson(this);
    }

    private static Gson gson=new Gson();

    public ResultMsg(String content, Map<String, String> names) {
        this.content = content;
        this.names = names;
        this.status = SUCCESS_STATUS;
    }

    //连接成功
    public String ok(String content, String type,Map<String, String> names) {
        this.content = content;
        this.names = names;
        this.type = type;
        this.status = SUCCESS_STATUS;
        return gson.toJson(this);
    }

    public String ok(String content, Map<String, String> names) {
        this.content = content;
        this.names = names;
        this.type = type;
        this.status = SUCCESS_STATUS;
        return gson.toJson(this);
    }

    public String success(String name,String content, Map<String, String> names) {
        this.content = name+" "+DateFormat.getDateTimeInstance().format(date) +":<br/> "+content;
        this.names = names;
        this.status = SUCCESS_STATUS;
        this.type = SUCCESS_TYPE;
        return gson.toJson(this);
    }

    //连接成功
    public String success(String content, List<NettyUserInfo> users,String name) {
        this.content =" [" + name+"] "+DateFormat.getDateTimeInstance().format(date) +":<br/> "+content;
        Map<String, String> map = new HashMap<>();
        for (NettyUserInfo user : users){
            map.put(user.getUserId(),user.getUsername());
        }
        this.names = map;
        this.type = SUCCESS_TYPE;
        this.status = SUCCESS_STATUS;
        return gson.toJson(this);
    }





    public String error(String errMsg) {
        this.content = errMsg;
        this.status = ERROR_STATUS;
        return  gson.toJson(this);
    }

    public String all(String content) {
        this.content = content;
        this.type = SUCCESS_TYPE;
        this.status = SUCCESS_STATUS;
        return  gson.toJson(this);
    }

    public String all(String content,String type) {
        this.content = content;
        this.status = SUCCESS_STATUS;
        this.type = type;
        return  gson.toJson(this);
    }

    public ResultMsg() {
    }


}
