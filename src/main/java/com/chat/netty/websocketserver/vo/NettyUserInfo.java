package com.chat.netty.websocketserver.vo;

import io.netty.channel.ChannelHandlerContext;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

public class NettyUserInfo implements Serializable {

    private String id;
    private String username;
    private String password;
    private LocalDateTime created_date;
    private String groupId;



    public NettyUserInfo() {
    }

    public NettyUserInfo(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getCreated_date() {
        return created_date;
    }

    public void setCreated_date(LocalDateTime created_date) {
        this.created_date = created_date;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", created_date=" + created_date +
                '}';
    }

    public Map<String, ChannelHandlerContext> onlineUserMap; //用户信息

    public Map<String, ChannelHandlerContext> getOnlineUserMap() {
        return onlineUserMap;
    }

    public void setOnlineUserMap(Map<String, ChannelHandlerContext> onlineUserMap) {
        this.onlineUserMap = onlineUserMap;
    }

    public String getUserId() {
        return id+"";
    }
}
