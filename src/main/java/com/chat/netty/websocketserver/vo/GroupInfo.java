package com.chat.netty.websocketserver.vo;

import lombok.Data;

import java.util.List;

@Data
public class GroupInfo {
    public List<NettyUserInfo> users;
}
