package com.chat.netty.udpserver.udpHandler;



import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
@Sharable
public class UdpNettyServerHandler extends SimpleChannelInboundHandler<DatagramPacket> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UdpNettyServerHandler.class);

    /**
     * 描述：读取完连接的消息后，对消息进行处理。
     *      这里主要是处理Upd请求
     */
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, DatagramPacket packet) throws Exception {
        ByteBuf buf = packet.content();
        byte[] req = new byte[buf.readableBytes()];
        //复制内容到字节数组bytes
        buf.readBytes(req);
        String srt2 = new String(req, "GBK").trim();
        LOGGER.info("UdpServer数据: " + srt2);
        buf.release();
    }
}
