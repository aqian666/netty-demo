package com.chat.netty.config;


import com.chat.netty.handlercfg.NettyChildChannelHandler;
import com.chat.netty.udpserver.UdpNettyServer;
import com.chat.netty.websocketserver.WebSocketNettyServer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
public class RegisterConfig{

    @Resource(name = "nettyChildChannelHandler")
    private NettyChildChannelHandler nettyChildChannelHandler;

    @Resource(name = "udpNettyServerHandler")
    private ChannelHandler udpNettyServerHandler;

    @Value("${netty.websocket.port}")
    private int webSocketPort;

    @Value("${netty.udp.port}")
    private int updPort;
    @Value("${netty.udp.host}")
    private String udpHost;

    @Bean
    public WebSocketNettyServer getWebSocketServer() {
        WebSocketNettyServer webSocketServer = new WebSocketNettyServer();
        webSocketServer.setPort(webSocketPort);
        webSocketServer.setChildChannelHandler(nettyChildChannelHandler);
        return webSocketServer;
    }

    @Bean
    public UdpNettyServer getUdpSocketServer() {
        UdpNettyServer udpNettyServer = new UdpNettyServer();
        udpNettyServer.setPort(updPort);
        udpNettyServer.setHost(udpHost);
        udpNettyServer.setChildChannelHandler(udpNettyServerHandler);
        return udpNettyServer;
    }

    @Bean("bossGroup")
    public EventLoopGroup getBossGroup() {
        return new NioEventLoopGroup();
    }

    @Bean("workerGroup")
    public EventLoopGroup getWorkerGroup() {
        return new NioEventLoopGroup();
    }

    @Bean
    public ServerBootstrap getServerBootstrap(){
        return new ServerBootstrap();
    }


}
