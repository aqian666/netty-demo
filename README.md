![logo.png](img/logo.png)

# netty-chat -- 一个基于netty开发的在线聊天程序

- 支持一对一单独聊天
- 支持群聊功能
- 实现基于netty,UDP服务,可以收发消息
- ...


#### 核心依赖
| 依赖              | 版本    |
|-----------------|-------|
| Spring Boot     | 2.7.2 |
| Spring Boot JPA |   -   |
| MySQL           | 5.7.x |
| Netty           |   -   |
| freemarker      |   -   |

#### 运行效果图
![img.png](img/img.png)

![img.png](img/img1.png)


#### 其它说明
后续功能陆续开发ing

